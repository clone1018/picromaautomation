<?php

class Account extends Eloquent {

    public static function registerUnregistered() {
        $accounts = Account::where('created', false)->get();

        foreach($accounts as $account) {
            try {
                $urltopost = "https://picroma.com/account/create";
                $datatopost = array (
                    'username' => $account->username,
                    'email' => $account->email,
                    'password' => $account->password,
                    'confirmpassword' => $account->password,
                    'post' => 'Create Account',
                );

                $fields_string = '';
                foreach($datatopost as $key => $value) { $fields_string .= $key.'='.urlencode($value).'&'; }
                rtrim($fields_string, '&');

                $ch = curl_init ($urltopost);
                curl_setopt ($ch, CURLOPT_POST, true);
                curl_setopt ($ch, CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));


                curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);

                $returnBody = curl_exec ($ch);
            } catch(Requests_Exception $e) {
                Log::info($e->getMessage());
                continue;
            }


            if(strpos($returnBody, 'An email with an activation link has been sent to') !== false) {
                Mail::send('emails.success', array('account' => $account), function($message) use ($account) {
                    $message->to($account->email)->subject('Picroma Account Created!');
                });

                $account->delete();
            } else {
                Log::error('Failed registering user', array('account' => $account->username));
            }
        }
    }

}