<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Picroma Account Created</h2>

        <div>
            We've created your account on Picroma, your login details are below: <br>
            <ul>
                <li>Username: {{{ $account->username }}}</li>
                <li>Email: {{{ $account->email }}}</li>
                <li>Password: {{{ $account->password }}}</li>
            </ul>
            <br>
            Also please make sure you wait for your Picroma verification email.

        </div>
    </body>
</html>