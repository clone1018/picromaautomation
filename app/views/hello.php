<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8 />
        <title>Picroma Tools</title>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Picroma Tools</h1>
            <p>Since Picroma has issues with stability (hire me!) we're providing a couple of tools to help you with getting access to the game.</p>
            <hr><br>

            <h2>Register a Picroma Account</h2>
            <p>By using this tool we'll automatically register your account details into Picroma and then send you an email with your information when your account is successfully registered.</p>

            <form class="form-horizontal" action='/picroma/account' method="POST">
                <fieldset>
                    <div id="legend">
                        <legend class="">Register</legend>
                    </div>
                    <div class="control-group">
                        <!-- Username -->
                        <label class="control-label"  for="username">Username</label>
                        <div class="controls">
                            <input type="text" id="username" name="username" placeholder="" class="input-xlarge" maxlength="20">
                            <p class="help-block">Username should be under 20 characters.</p>
                        </div>
                    </div>

                    <div class="control-group">
                        <!-- E-mail -->
                        <label class="control-label" for="email">E-mail</label>
                        <div class="controls">
                            <input type="text" id="email" name="email" placeholder="" class="input-xlarge" maxlength="40">
                            <p class="help-block">Email should be under 40 characters.</p>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <p>A password will be generated randomly for security and emailed to you when your account is created.</p>
                        </div>
                    </div>

                    <div class="control-group">
                        <!-- Button -->
                        <div class="controls">
                            <button class="btn btn-success">Register</button>
                        </div>
                    </div>
                </fieldset>
            </form>

            <hr>
            <h2>Buy Cube World</h2>
            <p>Coming Soon</p>

            <hr>
            <p><a href="https://bitbucket.org/clone1018/picromaautomation">Open Source</a></p>
        </div>
    </body>
</html>