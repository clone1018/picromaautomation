<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return View::make('hello');
});

Route::post('/picroma/account', function() {

    $rules = array(
        'username' => 'required|max:20|unique:accounts',
        'email' => 'required|max:40|email|unique:accounts'
    );

    $validator = Validator::make(Input::all(), $rules);
    if($validator->fails()) {
        return $validator->messages();
    }

    $account = new Account();

    $account->username = Input::get('username');
    $account->email = Input::get('email');
    $account->password = Str::random(10);
    $account->created = false;

    $account->save();

    return 'Account created, we\'ll register you at the next chance we get.';
});

Route::get('/status', function() {
    $online = false;
    try {
        $status = Requests::get('https://picroma.com/account/create')->body;
    } catch(Exception $e) {
        return Response::json($e);
    }

    if(strpos($status, 'This will be your nickname when posting comments.') !== false) {
        $online = true;
    }

    return Response::json(array(
        'account' => $online
    ));
});