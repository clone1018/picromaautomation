<?php

use Illuminate\Database\Migrations\Migration;

class CreatePicromaUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accounts', function($table) {
            $table->increments('id');

            $table->string('username', 20);
            $table->string('email', 40);
            $table->string('password')->nullable();
            $table->boolean('created');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}