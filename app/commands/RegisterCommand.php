<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RegisterCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'register';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Registers everyone.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
        try {
            $status = Requests::get('https://picroma.com/account/create')->body;
        } catch(Requests_Exception $e) {
            $this->error($e->getMessage());
            exit;
        }

        if(strpos($status, 'This will be your nickname when posting comments.') !== false) {
            Account::registerUnregistered();
            $this->info('Created Accounts');
        } else {
            $this->error('Website is not accepting registrations.');
            exit;
        }


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}